//Functions
const $ = (id) => document.querySelector(id);
const $$ = (id) => document.querySelectorAll(id);
const ckeckTokenExpiration = (token) => {
    try {
        const data = jwtDecode(token);
        if (Date.now() >= data?.exp * 1000) {
            return true;
        }
        return false;
    }
    catch (err) {
    }
    return -1;
}
//Global Variables
const API_URL = "http://d.taieldeluca.com.ar:81/shorten";
const USER_TOKEN = sessionStorage.getItem('user_token');
